(function($) {		
			
			/* <![CDATA[ */
			
			/* ---------------------------------------------------------------------- */
			/*	Custom Function
			/* ---------------------------------------------------------------------- */
			
			/* inialize custom scroll */
			function comment_enscroll() {
				$('.comment_list').enscroll({
					showOnHover: true,
					verticalTrackClass: 'track3',
					verticalHandleClass: 'handle3'
				});
			};
			
			/* portfolio thumbnail hover */
			$('.wooz_works .portfolio .wooz-thumbnail').mouseenter( function(){
				$(this).find('span.roll').stop().fadeIn();
			}).mouseleave(function(){
				$(this).find('span.roll').stop().fadeOut();
			});
			
			/*portfolio thumbnail desc */
			$('.wooz_works .portfolio').each(function(){
					gears = $(this).find('.gear')
				
				$(gears).click( function(e){
					var row_col = $(this).parents('.col-xs-4');
					
					e.preventDefault();		
					$(row_col).find('div.roll-line').fadeToggle('fast');
					return false;
				});
				
			});		
				
		
			// INSTANTIATE MIXITUP

			$('#Grid').mixitup({
				effects: ['fade', 'blur']
			});
			
		
			 /*Skill Bar*/
			if ($.fn.waypoint) {
				$('.skillbar').waypoint(function() {
						$('.skillbar').each(function(){
							percent = $(this).attr('data-percent');
							percent = percent + '%';
							
							$(this).find('.skillbar-bar').animate({
								width: percent
							},1500);
						});
					}, { offset: 'bottom-in-view' });
					
					$('.is-animated').waypoint(function() {
						$(this).removeClass('is-animated').addClass('element-animate')
					}, { offset: 'bottom-in-view' });
			}
			
			//Check if element exists in jQuery
			$.fn.doesExist = function(){
				return $(this).length > 0;
			};
				
			/* ---------------------------------------------------------------------- */
			/*	Main Slider
			/* ---------------------------------------------------------------------- */
			$('#da-slider').cslider();	
			
				
			/* ---------------------------------------------------------------------- */
			/*	inalize easy zoom
			/* ---------------------------------------------------------------------- */
			function easyzoom_inalize(){
				var options = {
					id: 'zoom-panel',
					parent: 'div.zoom-container',
					error: '<p class="zoom-error">There has been a problem attempting to loading the image.</p>',
					loading: '<p class="fullsize-loading">Loading</p>',
					cursor: 'crosshair',
					touch: true
				};
				$('.zoom-target').easyZoom(options);
			}
			
			/* ---------------------------------------------------------------------- */
			/*	SearchBox Header Animate
			/* ---------------------------------------------------------------------- */	
			
			//if in result page/single page disable animate
			var content_area = $('#main.content-area').doesExist();
			if( content_area !== true ){
				$('.search-header .button').mouseenter( function(){
				
					$(this).css('background-color','#d46f59');				
					$('.search-header input[type="text"]').show();
					$('.search-header').css('z-index','999');
					
					$('.search-header input[type="text"]').stop().animate({
						width: 432,
						opacity: 1
					}, {
						duration:800,
						
					});
				
				});
				$('.search-header').mouseleave( function(){
					$(this).find('.button').css('background-color','transparent');
					$(this).css('z-index','0');
					$(this).find('input[type="text"]').stop().animate({
						width: 0,
						opacity:0
					}, {
						duration: 800
					});
				
				});
			}
			
			/* ---------------------------------------------------------------------- */
			/*	Owl slide caraousel
			/*	Responsive layout, resizing the items
			/* ---------------------------------------------------------------------- */			
			function vals_owl_slide(){
				$('.carousel').each(function(){
					var item_perslide = $(this).attr('data-item'),
						index_slide = $(this).attr('data-carousel'),
						owl_carousel = $(".carousel" + index_slide),
						pagination_next  = ".carousel-control-next" + index_slide,
						pagination_prev  = ".carousel-control-prev" + index_slide,
						pagination_first = ".carousel-control-first" + index_slide,
						pagination_last = ".carousel-control-last" + index_slide,
						last_item = $(this).find('.slide-carousel').length ;
					
					// Using custom configuration
					owl_carousel.owlCarousel({
						items : item_perslide,
						responsive : false,
					});
					
					 // Custom Navigation Events
					$(pagination_next).click(function(){
						owl_carousel.trigger('owl.next');
						return false;
					})
					$(pagination_prev).click(function(){
						owl_carousel.trigger('owl.prev');
						return false;
					})
					$(pagination_first).click(function(){
						owl_carousel.trigger('owl.goTo',0);
						return false;
					})
					$(pagination_last).click(function(){
						owl_carousel.trigger('owl.goTo',last_item);
						return false;
					})
				});
			}
			vals_owl_slide();
			
			
			/* ---------------------------------------------------------------------- */
			/*	Scroll to Top
			/* ---------------------------------------------------------------------- */			
			
			$('#top-link').hide();
			$(window).scroll(function(){
				if ($(this).scrollTop() > 600) {
					$('#top-link').fadeIn();
				} else {
					$('#top-link').fadeOut();
				}
			});
		 
			$('#top-link, #logo').click(function(){
				$("html, body").animate({ scrollTop: 0 }, 600);
				return false;
			});	
			
			$('.navbar-nav.nav a').click(function(e){
				e.preventDefault();
				
				var nav_src = $(this).attr('href');
				//alert( nav_src );
				//$( '#about' ).animate({ scrollTop: 0 }, 600);
				$("html, body").delay(50).animate({scrollTop: $(nav_src).offset().top }, 900);
			});
			

			
			/* ---------------------------------------------------------------------- */
			/*	magnificPopup Init
			/* ---------------------------------------------------------------------- */
			// Clone HTML
			jQuery.fn.outerHTML = function() {
			  return jQuery('<div />').append(this.eq(0).clone()).html();
			};
			// Initialize popup as usual
			function magnificPopup_init(){
				
				$('.magnificPopup_image').each(function(){
					var comment = $(this).find('.description_portfolio').outerHTML(),
					image_big = $(this).find('.gallery-slide').attr('data-zoom');
					
					//type image
					$(this).magnificPopup({
						type: 'image',					
						delegate: 'a.gallery-slide', // the container for each your gallery items
						mainClass: 'mfp-with-zoom', // this class is for CSS animation below
						image: {
							markup: '<div class="mfp-figure">'+
										'<div class="center"><div class="mfp-close"></div>'+
										'<div class="zoom-container"><a class="zoom-target" href="#">'+
										'<div class="mfp-img">'+
										'</div></a></div></div>'+
										'<div class="mfp-bottom-bar">'+
										  '<div class="mfp-title"></div>'+
										  '<div class="mfp-counter"></div>'+
										  '<div class="mfp-counter"></div>'+
										'</div>'+
									  '</div>',
									  // Popup HTML markup. `.mfp-img` div will be replaced with img tag, `.mfp-close` by close button

							cursor: 'mfp-zoom-out-cur', // Class that adds zoom cursor, will be added to body. Set to null to disable zoom out cursor. 
							  
							titleSrc: function(item) {
								 return '<div class="side"><p class="title">' + item.el.attr('title') + '</p><div class="description">' + comment + ' </div></div>';
								},

							verticalGap:88,

							verticalFit: true, // Fits image in area vertically

							tError: '<a href="%url%">The image</a> could not be loaded.' // Error message
						},

						zoom: {
							enabled: true, // By default it's false, so don't forget to enable it

							duration: 300, // duration of the effect, in milliseconds
							easing: 'ease-in-out', // CSS transition easing function 

							// The "opener" function should return the element from which popup will be zoomed in
							// and to which popup will be scaled down
							// By defailt it looks for an image tag:
							opener: function(openerElement) {
								 // openerElement is the element on which popup was initialized, in this case its <a> tag
								 // you don't need to add "opener" option if this code matches your needs, it's defailt one.
								 return openerElement.is('img') ? openerElement : openerElement.find('img');
							}
							
						},
						
						callbacks: {
							imageLoadComplete: function() {
								// fires when image in current popup finished loading
								// avaiable since v0.9.0
								
								comment_enscroll();
								easyzoom_inalize();
							},
							change: function(item) {
								// Direct reference to your popup element	
								$(this.content).find('a.zoom-target').attr('href',item.el.attr('data-zoom') );
							}						
						}

					});
				});
				
				//type video or iframe
				$('.magnificPopup_video').magnificPopup({
					type: 'iframe',
					  
					  
					iframe: {
						 markup: '<div class="mfp-iframe-scaler">'+
									'<div class="mfp-close"></div>'+
									'<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
								  '</div>', 
								// HTML markup of popup, `mfp-close` will be replaced by the close button
						patterns: {
							youtube: {
								  index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

								  id: 'v=', // String that splits URL in a two parts, second part should be %id%
								  // Or null - full URL will be returned
								  // Or a function that should return %id%, for example:
								  // id: function(url) { return 'parsed id'; } 

								  src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
							},
							vimeo: {
								index: 'vimeo.com/',
								id: '/',
								src: 'http://player.vimeo.com/video/%id%?autoplay=0'
							},
							gmaps: {
								index: 'http://maps.google.',
								src: '%id%&output=embed'
							},
							dailymotion: {
						   
								index: 'dailymotion.com',
								
								id: function(url) {        
									var m = url.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);
									if (m !== null) {
										if(m[4] !== undefined) {
										  
											return m[4];
										}
										return m[2];
									}
									return null;
								},
								
								src: 'http://www.dailymotion.com/embed/video/%id%'
							
							}
						}
					} 
			  
				});
				
				//type gallery
				$('.magnificPopup_gallery').each(function() { 
					//clone comments
					var comment = $(this).find('.description_portfolio').outerHTML(),
					image_big = $(this).find('.gallery-slide').attr('data-zoom');
				
					// the containers for all your galleries should have the class gallery
					$(this).magnificPopup({
						delegate: 'a.gallery-slide', // the container for each your gallery items
						type: 'image',
						mainClass: 'mfp-with-zoom', // this class is for CSS animation below
						image: {
							  markup: '<div class="mfp-figure">'+
										'<div class="center"><div class="mfp-close"></div>'+
										'<div class="zoom-container"><a class="zoom-target" href="#">'+
										'<div class="mfp-img">'+
										'</div></a></div></div>'+
										'<div class="mfp-bottom-bar">'+
										  '<div class="mfp-title"></div>'+
										  '<div class="mfp-counter"></div>'+
										  '<div class="mfp-counter"></div>'+
										'</div>'+
									  '</div>', // Popup HTML markup. `.mfp-img` div will be replaced with img tag, `.mfp-close` by close button

							  cursor: 'mfp-zoom-out-cur', // Class that adds zoom cursor, will be added to body. Set to null to disable zoom out cursor. 
							  
							  titleSrc: function(item) {
								 return '<div class="side"><p class="title">' + item.el.attr('title') + '</p><div class="description">' + comment + ' </div></div>';
								},

							  verticalGap:88,

							  verticalFit: true, // Fits image in area vertically

							  tError: '<a href="%url%">The image</a> could not be loaded.' // Error message
						},

						zoom: {
							enabled: true, // By default it's false, so don't forget to enable it

							duration: 300, // duration of the effect, in milliseconds
							easing: 'ease-in-out'
							
						},
						gallery: {
							enabled: true, // set to true to enable gallery

							preload: [1,2], // read about this option in next Lazy-loading section

							navigateByImgClick: true,

							arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button

							tPrev: 'Previous (Left arrow key)', // title for left button
							tNext: 'Next (Right arrow key)', // title for right button
							tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
						},
						
						callbacks: {
							imageLoadComplete: function() {
								// fires when image in current popup finished loading
								// avaiable since v0.9.0
								
								comment_enscroll();
								easyzoom_inalize();
							},
							change: function(item) {
								// Direct reference to your popup element	
								$(this.content).find('a.zoom-target').attr('href',item.el.attr('data-zoom') );
							}						
						}

					});
				}); 
			}
			if(jQuery().magnificPopup) {
				magnificPopup_init();
			}
			
			/* ---------------------------------------------------------------------- */
			/*	Layout Filter Quicksand
			/* ---------------------------------------------------------------------- 	
			function portfolio_quicksand() {
				
				// Setting Up Our Variables
				var $filter;
				var $container;
				var $containerClone;
				var $filterLink;
				var $filteredItems
				
				// Set Our Filter
				$filter = $('#filter_by li.active a').attr('class');
				
				// Set Our Filter Link
				$filterLink = $('#filter_by li a');
				
				// Set Our Container
				$container = $('.carousel .portfolio .owl-item');
				
				// Clone Our Container
				$containerClone = $container.clone();
				
				// Apply our Quicksand to work on a click function
				// for each for the filter li link elements
				$filterLink.click(function(e) 
				{
					// Remove the active class
					$('#filter_by li').removeClass('active');
					
					// Split each of the filter elements and override our filter
					$filter = $(this).attr('class');
					
					// Apply the 'active' class to the clicked link
					$(this).parent().addClass('active');
					
					// If 'all' is selected, display all elements
					// else output all items referenced to the data-type
					if ($filter == 'all') {
						$filteredItems = $containerClone.find('.slide-carousel.data1 div.portfolio_item'); 
					}
					else {
						$filteredItems = $containerClone.find('.slide-carousel.data1 div.portfolio_item[data-type~=' + $filter + ']'); 
					}
					
					// Finally call the Quicksand function
					$container.quicksand($filteredItems, 
					{
						// The Duration for animation
						duration: 350,
						// the easing effect when animation
						easing: 'swing',
						// height adjustment becomes dynamic
						adjustHeight: false,
						useScaling: false
					});
					 
					//Initalize our PrettyPhoto Script When Filtered
					$container.quicksand($filteredItems, 
						function () { 
						magnificPopup_init(); 
						//vals_owl_slide();
						}
					);	
					
					return false;
				});
				
				
			}if(jQuery().quicksand) {
					portfolio_quicksand();	
			}*/

			
		})(jQuery);